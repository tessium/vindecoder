from django.contrib import admin
from .models import *


class VehicleAdmin(admin.ModelAdmin):
    list_display = ['vin', 'year']
    list_filter = ['year']


admin.site.register(Vehicle, VehicleAdmin)
