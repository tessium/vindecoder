from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from .models import *
import requests
from vindecoder.settings import DECODE_URL


# func used to get data from list
def list_get(l, key):
    try:
        return l[key]
    except KeyError:
        return '-'


class DecodeView(GenericAPIView):

    def get(self, request, vin):
        # check if there such vin in db, so that we can return it not using 3rd party api
        exists = Vehicle.objects.filter(vin__iexact=vin).exists()
        if not exists:
            try:
                resp = requests.get(url=DECODE_URL.format((vin,)))
            except ConnectionError:
                return Response(status=200, data={'Error': 'True'})
            data = resp.json()

            # if vin is found
            if data['decode']['Valid'] == 'True':
                equip = data['decode']['vehicle'][0]
                year = equip['year']
                make = equip['make']
                model = equip['model']
                type_trim = list_get(equip, 'trim')

                # initialize with default optional data
                color = weight_auto = weight_manual = length = width = height = '-'

                for i in equip['Equip']:
                    if i['name'] == 'Color':
                        color = i['value']
                    if i['name'] == 'Curb Weight-automatic':
                        weight_auto = i['value']
                    if i['name'] == 'Curb Weight-manual':
                        weight_manual = i['value']
                    if i['name'] == 'Overall Length':
                        length = i['value']
                    if i['name'] == 'Overall Width':
                        width = i['value']
                    if i['name'] == 'Overall Height':
                        height = i['value']

                # add to db
                Vehicle.objects.create(vin=vin,
                                       year=year,
                                       make=make,
                                       model=model,
                                       type=type_trim,
                                       color=color,
                                       length=length,
                                       width=width,
                                       height=height,
                                       weight_auto=weight_auto,
                                       weight_manual=weight_manual)
                resp_data = {
                    'Error': 'False',
                    'valid': 'True',
                    'data': {
                        'vin': vin,
                        'year': year,
                        'make': make,
                        'model': model,
                        'type': type_trim,
                        'color': color,
                        'length': length,
                        'width': width,
                        'height': height,
                        'weight_auto': weight_auto,
                        'weight_manual': weight_manual,
                    }
                }
                return Response(status=200, data=resp_data)
            else:
                resp_data = {
                    'Error': 'False',
                    'valid': 'False',
                    'data': {}
                }
                return Response(status=200, data=resp_data)
        else:
            # if found in db return it
            obj = Vehicle.objects.get(vin=vin)
            resp_data = {
                'Error': 'False',
                'valid': 'True',
                'data': {
                    'vin': vin,
                    'year': obj.year,
                    'make': obj.make,
                    'model': obj.model,
                    'type': obj.type,
                    'color': obj.color,
                    'length': obj.length,
                    'width': obj.width,
                    'height': obj.height,
                    'weight_auto': obj.weight_auto,
                    'weight_manual': obj.weight_manual,
                }
            }
            return Response(status=200, data=resp_data)
