from rest_framework.test import APITestCase
from .models import Vehicle


class DecodeTestCase(APITestCase):
    def setUp(self):
        # use existing vin so that will be saved in db
        self.client.get('http://localhost/decode/1P3EW65F4VV300946')

    def test_vin_saved_db(self):
        obj = Vehicle.objects.last()
        # test whether vin in setup is saved
        self.assertEqual(obj.vin, '1P3EW65F4VV300946')

    def test_true_data(self):
        resp = self.client.get('http://localhost/decode/1g6dj1ed5b0143824')
        data = resp.json()
        # test endpoint with existing vin
        self.assertEqual(data['valid'], 'True')

    def test_false_data(self):
        resp = self.client.get('http://localhost/decode/123123123')
        data = resp.json()
        # test endpoint with not existing vin
        self.assertEqual(data['valid'], 'False')
