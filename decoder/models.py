from django.db import models


class Vehicle(models.Model):
    vin = models.CharField(max_length=128)
    year = models.CharField(max_length=4)
    make = models.CharField(max_length=128)
    model = models.CharField(max_length=128)
    type = models.CharField(max_length=128)
    color = models.CharField(max_length=128, default='-')
    length = models.CharField(max_length=128, default='-')
    width = models.CharField(max_length=128, default='-')
    height = models.CharField(max_length=128, default='-')
    weight_auto = models.CharField(max_length=128, default='-')
    weight_manual = models.CharField(max_length=128, default='-')
